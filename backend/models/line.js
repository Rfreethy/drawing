var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var lineShema = mongoose.Schema({
    x1: Number,
    x2: Number,
    y1: Number,
    y2: Number,
    type: String,
    
}, {
    versionKey: false // to disable the "__v" attribute
});

module.exports = mongoose.model('line', lineShema);