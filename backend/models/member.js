var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var memberSchema = mongoose.Schema({

    name: String,
    referenceID: String,
}, {
    versionKey: false // to disable the "__v" attribute
});

module.exports = mongoose.model('member', memberSchema);