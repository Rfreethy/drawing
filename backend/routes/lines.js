var lineModel = require('../models/line');

exports.get = function (request, response) {//gets json for all students
    console.log('app.get(/lines)');
    
    lineModel.find(function (error, line) {
        if (error) {
            response.send({error: error});
        }
        else {
            response.json({line: line});
        }
    });
};

exports.getID = function (request, response) {//gets json for specified student(by ID)
    console.log('app.get(/lines/:line_id)');

    lineModel.findById(request.params.line_id, function (error, line) {
        if (error) {
            response.send({error: error});
        }
        else {
            response.send({line: line});
        };
    });
};

exports.post =  function (request, response) {
    console.log('app.post(/lines)');
    
    var newline = new lineModel({
        x1: request.body.line.x1,
        x2: request.body.line.x2,
        y1: request.body.line.y1,
        y2: request.body.line.y2,
    });
    newline.save(function (error) {
        console.log('saving to database: (' + newline.x1 + "," + newline.y1 + ") -> (" + newline.x2 + "," + newline.y2 + ")");
        if (error) {
            response.send({error: error});
        }
        else {
            response.status(201).json({line: newline});
        }
    });
}

exports.delete = function (request, response, next) {
    console.log('app.delete(/lines/line_id)');
    lineModel.findById(request.params.line_id, function (error, line) {
        lineModel.remove({}, function(err){
            if(err) console.log(err);
            response.status(200).json({line: line});
            next();
        });
    });
    
};

