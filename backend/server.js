

var app = require('express')();
var server = require('http').createServer();
var SocketServer = require('ws').Server;
var bodyParser = require('body-parser');

var port = 8082;
var socket = new SocketServer({server:server});

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/draw');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'https://draw-rfreethy.c9users.io:8080');
  	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  	res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    next();
});











/*<><><><><><><><><><><><><><><> HTTP REQUESTS <><><><><><><><><><><><><><><>*/



var line = require('./routes/lines');
app.get('/lines', line.get);
app.get('/lines/:line_id', line.getID);
app.post('/lines', line.post);
app.delete('/lines/:line_id', line.delete);

app.delete('/lines/:line_id', function(request, response){
  socket.broadcast("deletelines");
});

var memberModel = require('./models/member');

app.get('/members', function (request, response) {//gets json for all students
    console.log('app.get(/members)');
    
    memberModel.find(function (error, member) {
        if (error) {
            response.send({error: error});
        }
        else {
            response.json({member: member});
        }
    });
})

app.post('/members',  function (request, response) {
    console.log('app.post(/members)');
    
    var newMember = new memberModel({
        name: request.body.member.name,
    });
    newMember.save(function (error) {
        console.log('saving to database: ' + newMember.name);
        if (error) {
            response.send({error: error});
        }
        else {
            response.status(201).json({member: newMember});
        }
    });
});













/*<><><><><><><><><><><><><><><> SOCKET ACTIVITY <><><><><><><><><><><><><><><>*/


function Member(id, n, s) {
  var member = {
    clientID : id,
    name : n,
    socket : s
  }
  return member;
}
var roster = []


socket.broadcast = function(data) {
  this.clients.forEach(function(client){
    client.send(data)
  })
}



socket.on('connection', function(connection){
  console.log("Connection Recieved");
  
  connection.on('message', function(message){
    console.log("message");
    
    var content = message.split(",");

    if(content[0] == 'member'){
      var newMember = Member(content[1],content[2], connection);
      roster.push(newMember);
      socket.broadcast(message);
    }
    else if(content[0] != 'stillhere'){
      socket.broadcast(message);
    }
    else{
      //someone wants more time
    }
  });
  
  
  connection.on('close', function(code, message){
      var count = 0;
      roster.forEach(function(client){
        if(connection == client.socket){
          socket.broadcast('disconnect,' +client.clientID)
          roster.splice(count,1);
          
          deleteFromDB(client.clientID);
          
          
        }
        count++;
      })
  });
});

function deleteFromDB(client){
console.log("asdf");
    memberModel.findById(client, function (error, member) {
      memberModel.remove({_id: member._id}, function(error){
        if(error){
          console.log(error);
        }else{
          console.log(client + " removed from DB")
        }
      });
  });
}













server.on('request', app);


server.listen(port,function(){
  console.log("We're partying on port " + port);
});


